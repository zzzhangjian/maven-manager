NAME = maven-manager-web
VERSION=1.0

.PHONY: all

all: build

build:
	mvn clean -DskipTests package -am -pl $(NAME)
	cp maven-manager-web/src/main/resources/maven.xml docker
	cp $(NAME)/target/$(NAME)-$(VERSION)-jar-with-dependencies.jar docker
	docker build -t $(NAME):$(VERSION) --no-cache --rm docker

publish:
	docker tag $(NAME):$(VERSION) $(NAME):$(VERSION)
	docker tag $(NAME):$(VERSION) $(NAME):latest
	docker push $(NAME)

run:
	docker run --name maven-manager-web --privileged=true -v /etc/localtime:/etc/localtime:ro -p 8880:8880 --rm $(NAME):$(VERSION)
