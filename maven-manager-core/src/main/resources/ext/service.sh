#!/bin/bash
BASE_DIR=`cd $(dirname $0); pwd -P`
MAVEN_MANGER_HOME=$BASE_DIR

returnValue=0
cd $MAVEN_MANGER_HOME
jarFile=maven-manager-web-1.0.war

startServer(){
	echo "start $MAVEN_MANGER_HOME"
	nohup java -jar $MAVEN_MANGER_HOME/$jarFile debug -Dconfig=maven.xml &
	returnValue=$?
}

stopServer(){
	echo "stop $MAVEN_MANGER_HOME"
	pidList=`ps -ef |grep "$MAVEN_MANGER_HOME/$jarFile"|grep -v "grep"|awk '{print $2}'`
	if [ -n "$pidList" ]; then
		kill -9 $pidList
	fi
	returnValue=0
}

case "$1" in
  start)
    startServer
    ;;
  stop)
    stopServer
    ;;
  restart)
    stopServer
    startServer
    ;;
  *)
    echo $"Usage: $0 {start|stop|restart}"
    returnValue=1
esac

exit $returnValue
