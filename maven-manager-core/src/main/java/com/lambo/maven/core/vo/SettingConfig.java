package com.lambo.maven.core.vo;

import com.lambo.maven.core.utils.AssertUtil;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;

/**
 * 配置文件.
 * Created by lambo on 2018/06/16.
 */
@XmlRootElement(name = "setting")
public class SettingConfig {
    //使用的域名
    public String httpDomain = "localhost";
    //使用的端口号
    public Integer httpPort = 8880;
    //项目名称
    public String httpContext = "/maven";
    //仓库本地的缓存位置, 只支持>${user.home}的替换
    public String localRepository = "${user.home}/.maven";
    //仓库列表.
    public List<Repository> repository;
    //用户.
    public List<User> user;
    //可用的仓库.
    public List<String> activeProfiles;

    public Repository getRepository(String libName) {
        for (Repository repository : this.repository) {
            if (repository.id.equals(libName)) {
                return repository;
            }
        }
        return null;
    }

    @XmlRootElement(name = "user")
    public static class User {
        //用户名.
        public String name;
        //密码.
        public String password;
    }

    @XmlRootElement(name = "repository")
    public static class Repository {
        // 仓库id, 很重要.
        public String id;
        // 仓库名称.
        public String name;
        // 模式. 0. 不可用, 2. 只写, 4. 只读 6.可读可写.
        public int mode = 4;
        // 转发到本地其它仓库中.
        public String target;
        // 转发异常,请求会转发到指定目录.
        public List<String> url;
    }

    public void init() {
        //初始化验证参数.并处理一些默认配置.
        AssertUtil.isTrue(httpPort > 0, "httpPort not config");
        AssertUtil.notNull(httpContext, "httpContext not config");
        this.httpContext = httpContext.endsWith("/") ? httpContext :httpContext + "/";
        AssertUtil.notNull(localRepository, "localRepository not config");
        this.localRepository = localRepository.replace("${user.home}", System.getProperty("user.home"));
        this.localRepository = localRepository.endsWith("/") ? localRepository.substring(0, localRepository.length() - 1) : localRepository;
        //activeProfiles取repository可用的仓库id.
        this.activeProfiles = repository.stream().filter(r -> r.mode > 0).map(r -> r.id).collect(Collectors.toList());
    }

    public static SettingConfig fromXML(byte[] xmlBytes) {
        try {
            JAXBContext context = JAXBContext.newInstance(SettingConfig.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(false);
            dbf.setValidating(false);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new ByteArrayInputStream(xmlBytes));
            return unmarshaller.unmarshal(doc, SettingConfig.class).getValue();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
