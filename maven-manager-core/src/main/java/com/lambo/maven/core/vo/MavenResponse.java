package com.lambo.maven.core.vo;

/**
 * response.
 * Created by lambo on 2018/06/17.
 */
public class MavenResponse {

    /**
     * response 的状态.
     */
    private int statusCode;

    /**
     *  response 的状态信息.
     */
    private String statusMessage;

    /**
     * get时的body.
     */
    private byte[] body;

    public MavenResponse(int statusCode, byte[] body) {
        this(statusCode, String.valueOf(statusCode), body);
    }

    public MavenResponse(int statusCode, String statusMessage, byte[] body) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.body = body;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
