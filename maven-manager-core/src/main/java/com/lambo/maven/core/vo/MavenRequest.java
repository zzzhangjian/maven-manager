package com.lambo.maven.core.vo;

/**
 * 请求.
 * Created by lambo on 2018/06/17.
 */
public class MavenRequest {

    /**
     * maven，只使用 get, put方法.
     */
    private String method;

    /**
     * http请求地址.
     */
    private String path;

    /**
     * body 只有在put方法才会有body部分.
     */
    private byte[] body;

    /**
     * 是否生成md5与sha1文件.
     */
    private boolean generateMd5Sha1;

    public MavenRequest(String method, String path, byte[] body, boolean generateMd5Sha1) {
        this.method = method;
        this.path = path;
        this.body = body;
        this.generateMd5Sha1 = generateMd5Sha1;
    }

    public boolean isGenerateMd5Sha1() {
        return generateMd5Sha1;
    }

    public void setGenerateMd5Sha1(boolean generateMd5Sha1) {
        this.generateMd5Sha1 = generateMd5Sha1;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }
}
