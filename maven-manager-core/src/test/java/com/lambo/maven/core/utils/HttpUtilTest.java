package com.lambo.maven.core.utils;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Test;

public class HttpUtilTest {

    @Test
    public void doGet() throws IOException {
        byte[] data = HttpUtil.doGet("http://www.baidu.com");
        assert data != null;
        System.out.write(data);
    }
}