# maven-manager

## 项目介绍
  - java程序员专用maven私有仓库
  - 项目主要用于部署在cpu与内存小的环境中.如树莓派.
  - 安装的war包目前只有 114 kb, 请看项目附件

## 软件架构
  - 使用java自带httpServer.进行http协议的处理.
  - 使用com.lambo.los.http.utils.HeadersFilter进行http头的处理.
  - 使用模拟jsoup http client 的工具类com.lambo.los.http.client.HttpConnection 做http请求工具类.
  - 支持打包成可执行的war,支持工具类 com.lambo.los.kits.war.Handler.
  - 支持可配置运行工具类 com.lambo.los.kits.RunnableMainRunner,只要class继承 runnable, 就可方便的设置属性.-Dconfig=xx.xml 就可以设置属性 config 值 xx.xml .
  - 封装 xml 配置的处理工具类 com.lambo.los.kits.XmlKit.

## 安装教程
### 项目需要jdk环境,请自行安装.
### 下载打包.
```
git clone https://gitee.com/zlbroot/maven-manager.git
cd maven-manager
mvn clean -DskipTests package
```
### 修改配置文件 maven.xml
```
<setting>
    <!-- 使用的域名 -->
    <httpDomain>maven.localhost.com</httpDomain>
    <!-- 使用的端口号 -->
    <httpPort>8880</httpPort>
    <!-- 项目名称. -->
    <httpContext>/maven</httpContext>
    <!-- 仓库本地的缓存位置, 只支持>${user.home}的替换 -->
    <localRepository>${user.home}/.maven</localRepository>
    <!-- 将用于上传时权限验证. -->
    <user>
        <name>admin</name>
        <password>password</password>
    </user>
    <!-- 仓库节点 -->
    <repository>
        <!-- 仓库id.装作为url的一部分 -->
        <id>public</id>
        <!-- 名称 -->
        <name>ali yun nexus public</name>
        <!-- mode, 4为可读， 2为可写，6为 可读写， 0 为仓库不可用. 默认为只读 4. -->
        <mode>4</mode>
        <!-- 代理到 ali maven center -->
        <url>http://maven.aliyun.com/nexus/content/repositories/public</url>
        <url>http://mvnrepository.com/artifact</url>
    </repository>
    <repository>
        <id>libs-snapshot-local</id>
        <name>libs-snapshot-local</name>
        <mode>4</mode>
    </repository>
    <repository>
        <id>libs-release-local</id>
        <name>libs-release-local</name>
        <mode>2</mode>
    </repository>
    <repository>
        <id>libs-snapshot</id>
        <name>libs-snapshot</name>
        <mode>4</mode>
        <!-- 转发到其它仓库地址 -->
        <target>libs-snapshot-local</target>
    </repository>
    <repository>
        <id>libs-release</id>
        <name>libs-release</name>
        <mode>4</mode>
        <!-- 转发到其它仓库地址 -->
        <target>libs-release-local</target>
    </repository>
</setting>

```


## 环境配置写在maven的配置文件中.setting.xml .
```
   <!-- 这里是需要上传的账号密码, -->
    <servers>
        <server>
            <id>lambo-release</id>
            <username>admin</username>
            <password>password</password>
        </server>
        <server>
            <id>lambo-snapshot</id>
            <username>admin</username>
            <password>password</password>
        </server>
    </servers>
```

## 项目配置,可以参见 maven-manager/pom.xml 配置.
```
    <distributionManagement>
        <repository>
            <id>lambo-release</id>
            <name>lambo-release</name>
            <url>http://localhost:8880/maven/libs-release-local</url>
        </repository>
        <snapshotRepository>
            <id>lambo-snapshot</id>
            <name>lambo-snapshot</name>
            <url>http://localhost:8880/maven/libs-snapshot-local</url>
        </snapshotRepository>
    </distributionManagement>
```


## 使用说明 .
```
将maven-manager-web/target/maven-manager-web-1.0-jar-with-dependencies copy过来 或者 直接下载项目附件.
java -jar maven-manager-web-1.0-jar-with-dependencies.jar 

指定配置文件为
java -jar maven-manager-web-1.0-jar-with-dependencies.jar  maven.xml

打开浏览器访问 http://localhost:8880/maven/libs-snapshot/
```