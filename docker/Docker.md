# How to build the docker image

```make build```

# How to publish the docker image

```make publish```

# Run the container

```make run```

# Run within Docker Compose

```
maven-manager-web:
  image: maven-manager-web
  ports:
    - "8880:8880"
```